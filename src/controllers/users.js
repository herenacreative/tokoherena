import UsersModel from '../models/users';
import Response from '../helpers/response';
// import { hashPassword } from '../helpers/security';

class UsersController {
    constructor() {
        console.log('Users controller loaded!');
    };

    async getAllUsers(req, res){
        let search= req.query.search || '';
        const sortBy = req.query.sortBy || 'fullname';
        const sortType = req.query.sortType || 'asc';
        const limit = parseInt(req.query.limit) || 100;
        const page = parseInt(req.query.page) || 1;

        try{
            const result = await UsersModel.getAllUsersModel(search, sortBy, sortType, limit, page)
            console.log(result)
            if(result){
                return new Response(res, result, 'Success get All Users', 200, 'success')
            }else{
                return new Response(res, null, 'Not Find Users', 404, 'failed')
            }
        }catch(error){
            console.log(error);
            return new Response(res, null, 'internal Server Error', 500, 'failed');
        };
    };

    async getIdUser(req, res) {
        const id = req.params.id
        try {
            const result = await UsersModel.getIdUserModel(id);
            if(result.length == 1){
                return new Response(res, result, `Success Get User ID ${id}`, 200, 'success')
            }
            return new Response(res, null, `Data Not Found`, 404, 'failed')
        } catch (error) {
            console.log(error);
            return new Response(res, null, 'internal Server Error', 500, 'failed')
        }
    }

};

export default new UsersController();
