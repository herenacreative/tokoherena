import ProductsModel from '../models/products';
import Response from '../helpers/response';

class ProductsController {
    constructor() {
        console.log('Products controller loaded!');
    };

    async getAllProducts(req, res){
        let search= req.query.search || '';
        const sortBy = req.query.sortBy || 'name';
        const sortType = req.query.sortType || 'asc';
        const limit = parseInt(req.query.limit) || 100;
        const page = parseInt(req.query.page) || 1;

        try{
            const result = await ProductsModel.getAllProductsModel(search, sortBy, sortType, limit, page)
            console.log(result)
            if(result){
                return new Response(res, result, 'Success get All Products', 200, 'success')
            }else{
                return new Response(res, null, 'Not Find Products', 404, 'failed')
            }
        }catch(error){
            console.log(error);
            return new Response(res, null, 'internal Server Error', 500, 'failed');
        };
    };

    async getIdProduct(req, res) {
        const id = req.params.id
        try {
            const result = await ProductsModel.getIdProductModel(id);
            if(result.length == 1){
                return new Response(res, result, `Success Get Product ID ${id}`, 200, 'success')
            }
            return new Response(res, null, `Data Not Found`, 404, 'failed')
        } catch (error) {
            console.log(error);
            return new Response(res, null, 'internal Server Error', 500, 'failed')
        }
    }

};

export default new ProductsController();
