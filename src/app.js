import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import dotenv from "dotenv";
import morgan from "morgan";
import database from "./helpers/databases";
import routes from "./routes";
import {createServer} from 'http';
import config from "./configs";

dotenv.config();
class App {
  app;

  constructor() {
    this.app = express();
    this.static();
    this.plugins();
    this.routes();
    this.error();
  }

  init() {
    const httpServer = createServer(this.app);
    const port = process.env.PORT;
    httpServer.listen(port, () => {
      this.connectdb();
      console.log(`connected to the server on port ${port}`);
    });
  }

  static() {
    this.app.use(`/${config.rootProjectPath}`, express.static("src/assets"));
  }

  plugins() {
    this.app.use(cors());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
    this.app.use(morgan("dev"));
  }

  routes() {
    this.app.use(`/${config.rootProjectPath}/api/v1`, routes);
  }

  error() {
    this.app.use((req, res, next) => {
      const error = new Error("Not Found");
      error.status = 404;
      next(error);
    });
    this.app.use((error, req, res, next) => {
      res.status(error.status || 500);
      res.json({
        error: {
          status: error.status,
          message: error.message,
        },
      });
    });
  }

  connectdb() {
    database.connect((error) => {
      if (error) throw error;
      console.log("Database Connected!");
    });
  }
}

const port = 8080;
const app = new App(port);
app.init();
