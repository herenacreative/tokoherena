import dotenv from "dotenv";

dotenv.config();
const config = {
  mysql: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
  },
  jwtSecretKey: process.env.JWT_KEY,
  jwtTokenLoginLifeTime: "1d",
  jwtTokenLoginRefreshTime: "7d",
  rootProjectPath: "tokoherena",
  imageUrlPath: (req) => {
    return `${req.protocol}://${req.get("host")}/${rootProjectPath}/images/`;
  },
};

export default config;
