import express from "express";
import UsersController from "../controllers/users";
import AuthMiddleware from "../middleware/auth";

class UsersRouter {
  router;
  auth;

  constructor() {
    this.router = express.Router();
    this.routes();
    this.auth = new AuthMiddleware();
  }

  routes() {
    this.router.get(
      "/",
      (req, res, next) => this.auth.verifyJwtToken(req, res, next),
      (req, res, next) => this.auth.checkRole(["admin"])(req, res, next),
      UsersController.getAllUsers
    );
    this.router.get(
      "/:id",
      (req, res, next) => this.auth.verifyJwtToken(req, res, next),
      (req, res, next) => this.auth.checkRole(["admin", "customer"])(req, res, next),
      UsersController.getIdUser
    );
  }
}

export default new UsersRouter().router;
