import express from "express";
import ProductsController from "../controllers/products";
import AuthMiddleware from "../middleware/auth";

class UsersRouter {
  router;
  auth;

  constructor() {
    this.router = express.Router();
    this.routes();
    this.auth = new AuthMiddleware();
  }

  routes() {
    this.router.get(
      "/",
      (req, res, next) => this.auth.verifyJwtToken(req, res, next),
      (req, res, next) => this.auth.checkRole(["admin", "customer"])(req, res, next),
      ProductsController.getAllProducts
    );
    this.router.get(
      "/:id",
      (req, res, next) => this.auth.verifyJwtToken(req, res, next),
      (req, res, next) => this.auth.checkRole(["admin", "customer"])(req, res, next),
      ProductsController.getIdProduct
    );
  }
}

export default new UsersRouter().router;
