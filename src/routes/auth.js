import express from 'express';
import AuthController from '../controllers/auth';

class AuthRouter{
    router;

    constructor(){
        this.router = express.Router();
        this.routes();
    }

    routes(){
        this.router.post('/login', AuthController.login)
        this.router.post('/register', AuthController.register)
    }
}

export default new AuthRouter().router;
