import express from 'express';
import UsersRouter from './users';
import AuthRouter from './auth';
import ProductRouter from './products';

class Router{
    constructor(){
        this.router= express.Router();
        this.routes();
    }

    routes(){
        this.usersRouter();
        this.authRouter();
        this.productRouter();
    }

    usersRouter(){
        this.router.use("/users", UsersRouter)
    }

    authRouter(){
        this.router.use("/auth", AuthRouter)
    }

    productRouter(){
        this.router.use("/products", ProductRouter)
    }
}

const routes = new Router().router;

export default routes;
