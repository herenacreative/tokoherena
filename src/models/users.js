import connection from "../helpers/databases";

class UsersModel {
  getAllUsersModel(search, sortBy, sortType, limit, page) {
    return new Promise((resolve, reject) => {
      // const sqlQuery = `SELECT * FROM users WHERE (phone like '%${search}%' OR fullname like '%${search}%') GROUP BY id order by ${sortBy} ${sortType} limit ${limit} offset ${page}`;
      const sqlQuery = `SELECT * FROM users`;
      console.log(sqlQuery)
      connection.query(
        sqlQuery,
        [search, sortBy, sortType, limit, page],
        (error, result) => {
          if (error) {
            console.log(error)
            reject(error);
          }
          resolve(result);
        }
      );
    });
  };

  getIdUserModel(id){
    return new Promise((resolve, reject) => {
      const sqlQuery = 'SELECT * FROM users WHERE id = ?'
      connection.query(sqlQuery, id, (err, result) => {
          if (err) {
              reject(err)
          }
          resolve(result)
      })
    })
  }  
}

export default new UsersModel();
