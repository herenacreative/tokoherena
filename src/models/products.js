import connection from "../helpers/databases";

class ProductsModel {
  getAllProductsModel(search, sortBy, sortType, limit, page) {
    return new Promise((resolve, reject) => {
      // const sqlQuery = `SELECT * FROM products WHERE (name like '%${search}%' OR description like '%${search}%') GROUP BY id order by ${sortBy} ${sortType} limit ${limit} offset ${page}`;
      const sqlQuery = `SELECT * FROM products`;
      console.log(sqlQuery)
      connection.query(
        sqlQuery,
        [search, sortBy, sortType, limit, page],
        (error, result) => {
          if (error) {
            console.log(error)
            reject(error);
          }
          resolve(result);
        }
      );
    });
  };

  getIdProductModel(id){
    return new Promise((resolve, reject) => {
      const sqlQuery = 'SELECT * FROM products WHERE id = ?'
      connection.query(sqlQuery, id, (err, result) => {
          if (err) {
              reject(err)
          }
          resolve(result)
      })
    })
  }  
}

export default new ProductsModel();
