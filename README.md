# tokoherena

application ecommerce

__endpoint api__:
- __[login]: (http://localhost:8080/tokoherena/api/v1/auth/login)

- __[register]: (http://localhost:8080/tokoherena/api/v1/auth/register)

- __[get-all-user]: (http://localhost:8080/tokoherena/api/v1/users)

- __[get-user-by-id]: (http://localhost:8080/tokoherena/api/v1/users/id)

---

"if role ADMIN, this can see get all endpoint",
"if role CUSTOMER, this can see get user by id"

---
## example for login:
email: mawar@mail.com (role admin)
password: 123

---

email: langit@mail.com (role customer)
password: 123

## file .env:
```
DB_HOST=localhost
DB_USER=root
DB_PASSWORD=4ntaHerena@
DB_DATABASE=tokoherena_db
JWT_KEY=1231ju213
PORT=8080
```

running endpoint ecommerce:
1. git clone `https://gitlab.com/herenacreative/tokoherena.git`
2. import database with name `tokoherena_db` in folder database
3. npm install
4. npm run start


