-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 27, 2021 at 10:36 AM
-- Server version: 8.0.25-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tokoherena_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `fullname` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` text,
  `address` varchar(255) DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('customer','admin') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `email`, `phone`, `address`, `username`, `password`, `role`, `created_at`, `updated_at`) VALUES
(1, 'sinta herena', 'sintaherena@gmail.com', '08381900500', 'bogor', 'herena_sb', '', 'customer', '2021-07-27 00:05:13', '2021-07-27 00:05:13'),
(2, 'ocean', 'ocean@mail.com', '083819000500', 'jakarta', 'ocean', '', 'customer', '2021-07-27 02:34:10', '2021-07-27 02:34:10'),
(3, 'mawar', 'mawar@mail.com', '08381999599', 'depok', 'mawar', '$2b$10$WmC/ftMxp8kLmwX2oiBNUem1dVl13LDIolBKM6bs8b/PuA48FzkSW', 'admin', '2021-07-27 03:25:36', '2021-07-27 03:25:36'),
(4, 'langit', 'langit@mail.com', '08381999599', 'bogor', 'langit', '$2b$10$J4gBATuea6usPGXP/dOtZeCx1OFYYn0ApCeX4MXE1GZggEhXCAkGi', 'customer', '2021-07-27 03:34:01', '2021-07-27 03:34:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
